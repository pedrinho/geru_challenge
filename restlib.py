import requests
import json


class QuoteAPI:
    def get_data(self):
        return dict(enumerate(self.quotes))

    def __init__(self):
        url = 'https://1c22eh3aj8.execute-api.us-east-1.amazonaws.com/challenge/quotes'
        r = requests.get(url)
        if r.status_code == requests.codes.ok:
            self.quotes = r.json()['quotes']


def get_quotes():
    q = QuoteAPI()
    return q.get_data()


def get_quote(index):
    q = QuoteAPI()
    return q.get_data()[index]
