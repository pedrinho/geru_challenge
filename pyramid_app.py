#!/usr/bin/env python
from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response
import restlib
from random import randint


def create_html(quotes):
    ret = '<ul>'
    for key, quote in quotes.items():
        ret += '<li>'
        ret += quote
        ret += '</li>'
    ret += '</ul>'
    return ret


def all_quotes(request):
    quotes = restlib.get_quotes()
    ret = create_html(quotes)
    return Response(ret)


def by_number(request):
    try:
        index = int(request.matchdict['quote_number'])
        return Response(restlib.get_quote(index))
    except ValueError:
        return Response('Erro ao converter quote number')
    except KeyError:
        return Response('Quote number invalido')


def random(request):
    quotes = restlib.get_quotes()
    index = randint(0, len(quotes))
    ret = str(index) + ' - ' + quotes[index]
    return Response(ret)


def home(request):
    return Response('Desafio Web 1.0')


if __name__ == '__main__':
    with Configurator() as config:
        config.add_route('home', '/')
        config.add_view(home, route_name='home')
        config.add_route('all_quotes', '/quotes')
        config.add_view(all_quotes, route_name='all_quotes')
        config.add_route('random', '/quotes/random')
        config.add_view(random, route_name='random')
        config.add_route('by_number', '/quotes/{quote_number}')
        config.add_view(by_number, route_name='by_number')
        app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 1234, app)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        print(' pressed, finalizing server...')
    server.server_close()
